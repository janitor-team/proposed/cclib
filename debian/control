Source: cclib
Section: science
Priority: optional
Maintainer: Debichem Team <debichem-devel@lists.alioth.debian.org>
Uploaders: Michael Banck <mbanck@debian.org>
Build-Depends: debhelper-compat (= 11),
               dh-python,
               python3-all,
               python3-numpy (>> 1:1.6.2~rc1-1~),
               python3-setuptools
Standards-Version: 4.5.0
Homepage: http://cclib.github.io
Vcs-Git: https://salsa.debian.org/debichem-team/cclib.git
Vcs-Browser: https://salsa.debian.org/debichem-team/cclib
Rules-Requires-Root: no

Package: cclib
Architecture: all
Depends: python3-cclib (= ${source:Version}),
         ${misc:Depends},
         ${python3:Depends}
Suggests: cclib-data
Description: Parsers and algorithms for computational chemistry
 A Python library that provides parsers for computational
 chemistry log files. It also provides a platform to implement
 algorithms in a package-independent manner.
 .
 This package contains helper scripts for end users.
 .
 If you are looking for the unit tests and data files managed by cclib,
 they are distributed separately as in non-free package cclib-data.

Package: python3-cclib
Architecture: all
Section: python
Provides: ${python3:Provides}
Depends: ${misc:Depends}, ${python3:Depends}
Recommends: cclib
Description: Parsers and algorithms for computational chemistry (Python3 module)
 A Python library that provides parsers for computational
 chemistry log files. It also provides a platform to implement
 algorithms in a package-independent manner.
 .
 This package contains the Python3 module.
